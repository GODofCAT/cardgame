package com.company;

import java.util.Scanner;

public class Archer extends WarriorClass {
    Scanner scanner = new Scanner(System.in);

    public Archer(double hp, double damage, int distance,int step,int attackRange) {
        super(hp, damage, distance,step,attackRange);
        this.setName("archer");
    }

    enum ability{
        rope_arrow,   //стрела на веревке - перемещает персонажа на 3 клетки назад
        double_shoot  //двойной выстрел - урон увеличивается в 2 раза
    }

    private ability getUsedAbility(){
        boolean hasAbility;
        ability usedAbility = null;
        int numOfAbility;

        do {

            numOfAbility = x.inputInt("1.double shoot\n2.rope arrow\n");


            if (numOfAbility == 1){
                usedAbility = ability.double_shoot;
                hasAbility=false;
            }
            else if (numOfAbility == 2){
                usedAbility = ability.rope_arrow;
                hasAbility=false;
            }
            else {
                hasAbility = true;
            }
        }while (hasAbility);

        return usedAbility;
    }

    private void useDoubleShoot(){
        double oldDm = this.getDamage();

        this.setDamage(oldDm*=2);
    }

    private void useRopeArrow(){
        int oldPosition = this.getPosition();

        this.setPosition(oldPosition-=3);
    }

    public void useAbility(){
        ability usedAbility = getUsedAbility();

        if (usedAbility == ability.rope_arrow){
            useRopeArrow();
        }
        else if (usedAbility == ability.double_shoot){
            useDoubleShoot();
        }
    }

}
