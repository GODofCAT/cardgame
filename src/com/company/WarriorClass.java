package com.company;

public abstract class WarriorClass {
    Scan x = new Scan();

    private String name;
    private double hp;
    private double damage;
    private int position;
    private int step;
    private int id;
    private int attackRange;
    private boolean abilityIsUse = false;

    public WarriorClass(double hp,double damage,int position,int step,int attackRange){
        this.hp = hp;
        this.damage = damage;
        this.position = position;
        this.step = step;
        this.attackRange = attackRange;
    }

    public int getAttackRange() {
        return attackRange;
    }

    public double getHp() {
        return hp;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setId(int id){this.id = id;}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getStep() {
        return step;
    }



    public void setName(String name) {
        this.name = name;
    }

    public double getDamage() {
        return damage;
    }

    public int getPosition() {
        return position;
    }

    public void setHp(double hp) {
        this.hp = hp;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public void setAbilityIsUse(boolean abilityIsUse) {
        this.abilityIsUse = abilityIsUse;
    }

    public boolean getAbilityIsUse(){
        return abilityIsUse;
    }

    public void move(int player){
        boolean error;
        do {
            error=false;

            String direction = x.inputStr("direction?: ").toLowerCase();

            if (direction.equals("forward")){
                position=player==1?+step:-step;
            } else if (direction.equals("back")) {
                position=player==1?-step:+step;
            }else {
                error = true;
            }


        }while (error);
    }

    public void useAbility() {}
}
