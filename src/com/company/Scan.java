package com.company;

import java.util.Scanner;

public class Scan {
    Scanner scanner = new Scanner(System.in);

    public int inputInt(String message){
        boolean error;
        int x = -1;
        do {
            System.out.print(message);

            error = false;

            try {
                x = scanner.nextInt();
            }catch (Exception e){
                error = true;
            }

        }while (error);
        return x;
    }

    public String inputStr(String message){
        System.out.println(message);
        String x = scanner.next();
        return x;
    }

    public boolean question(String question){
        boolean error;char yesORno;

        do {
            error=false;
            yesORno = inputStr(question+" (Y/N) \n").toUpperCase().charAt(0);
            if (yesORno == 'Y'){
                return true;
            }
            else if (yesORno == 'N'){
                return false;
            }
            else {
                error=true;
            }
        }while (error);
        return false;
    }
}
