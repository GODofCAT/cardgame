package com.company;

import java.util.Scanner;

public class Knight extends WarriorClass {

    Scanner scanner = new Scanner(System.in);

    public Knight(double hp, double damage, int distance,int step,int attackRange){
        super(hp, damage, distance,step,attackRange);
        this.setName("knight");
    }


    enum ability{
        deus_vult,   //желание бога - повышает здоровье на 2 и уыеличивает атаку на 1.5
        angel_wings  //ангельские крылья - перемещает рыцаря на 3 клетки
    }


    private ability getUsedAbility(){
        boolean hasAbility;
        ability usedAbility = null;
        int numOfAbility;

        do {

            numOfAbility = x.inputInt("1.angel wings\n2.deus vult\n");


            if (numOfAbility == 1){
                usedAbility = ability.angel_wings;
                hasAbility=false;
            }
            else if (numOfAbility == 2){
                usedAbility = ability.deus_vult;
                hasAbility=false;
            }
            else {
                hasAbility = true;
            }
        }while (hasAbility);

        return usedAbility;
    }

    private void useDeusVult(){
        double oldHp = this.getHp();
        double oldDm = this.getDamage();

        this.setHp(oldHp+=0.5);
        this.setDamage(oldDm+=1.5);
    }

    private void useAngelWings(){
        int oldPosition = this.getPosition();

        this.setPosition(oldPosition+=3);
    }

    public void useAbility(){
        ability usedAbility = getUsedAbility();

        if (usedAbility == ability.deus_vult){
            useDeusVult();
        }
        else if (usedAbility == ability.angel_wings){
            useAngelWings();
        }
    }

}
