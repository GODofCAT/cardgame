package com.company;

import com.company.Scan;
import com.company.WarriorList;

public class Main {

    static Scan x = new Scan();

    static void turn(int turn){
        if (turn==1){
            System.out.println("first player turn");
        }
        else if (turn==2){
            System.out.println("second player turn");
        }
    }
//---------------------MENU FUNC---------------------------//

    static void printMenu(){
        System.out.println("1.move");
        System.out.println("2.attack");
        System.out.println("3.use ability");
        System.out.println("4.skip");
    }

    static int getMenuPoint(){
        int point;
        boolean error;
        do {
            error = false;
            point = x.inputInt("input menu point");
            if (point<0||point>5||point%10!=0){
                error=true;
            }
        }while (error);

        return point;
    }

//---------------------MENU FUNC---------------------------//

//---------------------OTHER FUNC--------------------------//
    static void printXenters(int x){
        for (int i = 0; i < x; i++) {
            System.out.println();
        }
    }

    static void printWall(){
        for (int i = 0; i < 50; i++) {
            System.out.print('=');
        }
        System.out.println();
    }
//---------------------OTHER FUNC--------------------------//

//-----------------------GAME-----------------------------//
    static void playerTurn(WarriorList mainPlayer, WarriorList otherPlayer,int turn){
        for (int i = 0; i < mainPlayer.getLength(); i++) {

            System.out.println((i+1)+" character menu");
            System.out.println();

            printMenu();

            switch (getMenuPoint()){
                case 1:
                    mainPlayer.getElemByIndex(i).move(turn);
                    break;

                case 2:
                    otherPlayer.printSquad(otherPlayer.getPlayer());
                    attack(mainPlayer,otherPlayer,mainPlayer.getElemIdByIndex(i));
                    break;

                case 3:
                    if (!(mainPlayer.getElemByIndex(i).getAbilityIsUse())){
                        mainPlayer.getElemByIndex(i).useAbility();
                        mainPlayer.getElemByIndex(i).setAbilityIsUse(true);
                    }
                    else {
                        System.out.println("ability already used");
                    }
                    break;

                case 4:
                    break;
            }

            System.out.println();

        }
    }

    static void attack(WarriorList pAttack,WarriorList pAttacked,int attackId){
        System.out.println("who you wanna attack");

        int attackedId = pAttacked.getId();
        int distance = pAttack.getElemWithId(attackId).getPosition()+pAttacked.getElemWithId(attackedId).getPosition();


        if(distance<=pAttack.getElemWithId(attackedId).getAttackRange()){
            pAttacked.getElemWithId(attackedId).setHp(pAttacked.getElemWithId(attackedId).getHp()-pAttack.getElemWithId(attackId).getDamage());//attackedCharacter.hp = attackedCharacter.hp - attackCharacter.hp
            pAttacked.isDead(attackedId);
        }
        else {
            System.out.println("miss");
        }
    }

    static boolean allDie(WarriorList playerSquad){
        int countDieCharacter = 0;

        for (int i = 0; i < playerSquad.getLength(); i++) {
            if (playerSquad.getElemByIndex(i).getName() == "dead"){
                countDieCharacter++;
            }
        }
        return countDieCharacter==5;
    }

//-----------------------GAME-----------------------------//

    public static void main(String[] args) {

      WarriorList p1 = new WarriorList();
      WarriorList p2 = new WarriorList();

      p1.setPlayer(1);
      p2.setPlayer(2);

      boolean p1IsDie = false;
      boolean p2IsDie = false;

      //----game start----//

      System.out.println("create squad for p1");
      p1.createSquad();

      System.out.println();

      System.out.println("create squad for p2");
      p2.createSquad();

      System.out.println();
        printXenters(50);
      while (!p1IsDie&&!p2IsDie){
          //--p1 begin--//
          p1.printSquad(1);
          printWall();
          p2.printSquad(2);
          printXenters(2);

          turn(1);
          playerTurn(p1,p2,1);

          //--p1 finish--//

          printXenters(50);

          //--p2 begin--//

          p1.printSquad(1);
          printWall();
          p2.printSquad(2);
          printXenters(2);

          turn(2);
          playerTurn(p2,p1,2);

          printXenters(50);

          //--p2 finish--//

          p1IsDie = allDie(p1);
          p2IsDie = allDie(p2);
      }

      System.out.println();

      if (p1IsDie) {
          System.out.println("player 2 win");
      } else {
          System.out.println("player 1 win");
      }


    }
}
