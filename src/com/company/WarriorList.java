package com.company;

import java.util.ArrayList;
import java.util.List;

public class WarriorList {
    private Scan x = new Scan();

    private int id = 1;
    private int player;
    private WarriorClass[] squad = new WarriorClass[5];

    public void createSquad(){
        System.out.print("you should fill position \n for fill write number of warrior \n 1 - knight \n 2 - archer \n");

        int choice;

        for (int i = 1; i < squad.length+1; i++) {
            choice = x.inputInt(i+"/5 : ");

            if (choice == 1){
                squad[i-1] = new Knight(10,1.5, 5,2,2);;
                squad[i-1].setId(id);
                id++;
            }
            else if (choice == 2){
                squad[i-1] = new Archer(7,1, 5,3,6);
                squad[i-1].setId(id);
                id++;
            }
            else {
                i--;
            }
        }
    }

    public int getId(){
        int id;

        do {
            id = x.inputInt("input id : ");
        }while (id<1&&id>5);

        return id;
    }

    public void setPlayer(int player){
        this.player = player;
    }

    public int getPlayer(){
         return player;
    }

    public WarriorClass getElemById(){

        for (int i = 0; i < squad.length; i++) {
            if (squad[i].getId() == getId()-1){
                return squad[i];
            }
        }

        return null;
    }

    public WarriorClass getElemWithId(int id){
        for (int i = 0; i < squad.length; i++) {
            if (squad[i].getId() == id){
                return squad[i];
            }
        }

        return null;
    }

    public WarriorClass getElemByIndex(int index){
        return squad[index];
    }

    public int getElemIdByIndex(int index){
        return squad[index].getId();
    }

    public int getLength(){
        return squad.length;
    }

    public void isDead(int id){
        if (getElemWithId(id).getHp()<=0){
            getElemWithId(id).setName("dead");
        }
    }


    public void printSquad(int player){

        System.out.println("player "+player+" squad");

        System.out.printf("%-4s %-7s %-5s %-7s %-10s %-14s \n","id","name","hp","damage","position","attack range");

        for (int i = 0; i < squad.length; i++) {
            System.out.printf("%-4d %-7s %-6.1f %-7.1f %-10d %-14d\n" ,squad[i].getId() , squad[i].getName() , squad[i].getHp() , squad[i].getDamage() , squad[i].getPosition() , squad[i].getAttackRange());
        }
    }
}
